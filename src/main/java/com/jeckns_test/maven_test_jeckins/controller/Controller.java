package com.jeckns_test.maven_test_jeckins.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@RequestMapping(value="/home")
	public String getIndex()
	{
		System.err.println("Entro");
		return "Welcome";
	}
	
	
	@RequestMapping(value="/")
	public String getTest()
	{
		System.err.println("Sono nel map /");
		return "Welcome";
	}

}
