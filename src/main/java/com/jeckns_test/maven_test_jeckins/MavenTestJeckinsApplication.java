package com.jeckns_test.maven_test_jeckins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MavenTestJeckinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MavenTestJeckinsApplication.class, args);
	}

}

